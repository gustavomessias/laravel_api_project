<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(\App\Http\Controllers\CarrosController::class)->group(function () {
    Route::get('/list', 'index');
    Route::post('/create', 'create');
    Route::post('/update/{carro}', 'update');
    Route::delete('/delete/{carro}', 'delete');
    Route::get('/carro/{carro}', 'search');
});
