<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<p>* Desenvolva em no máximo 2 horas uma API (Rest Full) com as seguintes caracteristicas:</p>
<p>- Uma rota de consulta de carros;</p>
<p>- Uma rota de cadastro de carros;</p>
<p>- Uma rota de update de carros pelo ID;</p>
<p>Espera-se que a API seja desenvolvida com o uso do Framework Laravel @ultima_versao, banco de dados Mysql e uma tabela simples para gerar as migrations do Laravel:</p>
<p>** Campos esperados da tabela Carro: id, modelo, marca_fabricante, ano, valor_tabela. </p>

<p>*** Favor colocar no final Nome do candidato e data de resolução.</p>
</p>

## Author

Gustavo Messias

## Date

22/11/2022 18:00 hrs
