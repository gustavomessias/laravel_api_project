<?php

namespace App\Http\Controllers;

use App\Models\Carros;
use App\Models\Post;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CarrosController extends Controller
{
    public array $rules;

    public array $messages;

    public function __construct()
    {
        $this->rules = [
            'modelo'            => 'required',
            'marca_fabricante'  => 'required',
            'valor_tabela'      => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ];

        $this->messages = [
            'modelo.required'           => 'Preenchimento do campo modelo é obrigatório;',
            'marca_fabricante.required' => 'Preenchimento do campo marca_fabricante é obrigatório;',
            'valor_tabela.required'     => 'Preenchimento do campo valor_tabela é obrigatório ;',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $carros = Carros::all();

        return response()->json([
            'status' => true,
            'carros' => $carros
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $this->validate($request, $this->rules, $this->messages);

            $carro = new Carros();
            $carro->fill($request->all());
            $carro->save();

            return response()->json([
                'status' => true,
                'message' => 'Carro cadastrado com sucesso'
            ]);
        }catch (\Exception $exception){
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $carro
     * @return JsonResponse
     */
    public function search(int $carro)
    {
        $carro = Carros::find($carro);

        if($carro){
            return response()->json([
                'status' => true,
                'data' => $carro
            ]);
        }

        return response()->json([
            'status' => false,
            'message' => 'Nenhum carro encontrado com esse id'
        ], 400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Carros $carro
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request, Carros $carro)
    {
        try {
            $this->validate($request, $this->rules, $this->messages);
            $carro->fill($request->all());
            $carro->save();

            return response()->json([
                'status' => true,
                'message' => 'Carro editado com sucesso'
            ]);
        }catch (\Exception $exception){
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Carros $carro
     * @return JsonResponse
     */
    public function delete(Carros $carro)
    {
        try {
            $carro->delete();

            return response()->json([
                'status' => true,
                'message' => 'Carro deletado com sucesso'
            ]);
        }catch (\Exception $exception){
            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ], 400);
        }
    }

    public function validate(Request $request, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validated = $request->validate($rules, $messages);
    }
}
