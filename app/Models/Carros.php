<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Carro
 *
 * @property int $id
 * @property string $modelo
 * @property string $marca_fabricante
 * @property int $marca
 * @property float $valor_tabela
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class Carros extends Model
{
	protected $table = 'carros';

	protected $casts = [
		'marca' => 'int',
		'valor_tabela' => 'float'
	];

	protected $fillable = [
		'modelo',
		'marca_fabricante',
		'valor_tabela'
	];
}
